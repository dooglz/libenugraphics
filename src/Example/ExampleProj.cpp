// The main header for the graphics framework
#include <graphics_framework.h>

/* Rember to link with:
		libENUgraphics.lib;libnoise.lib;assimp.lib;
		FreeImage.lib;glew32s.lib;glfw3.lib;
		glfw3dll.lib;OpenGL32.lib; GLU32.lib
	You will also need assimp.dll, Freeimage.dll, glfw3.dll
*/

// The namespaces we are using
using namespace std;
using namespace graphics_framework;

// Our rendering code will go in here
bool render() { return true; }
int main(int argc, char *argv[]) {
  // Create application
  app application;
  // Set render function
  application.set_render(render);
  // Run application
  application.run();
  return 0;
}